package gitee.eillott.common.tools.date;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 日期期间处理
 *
 * @author eillott
 */
public class PeriodDateUtil {

    /**
     * yyyy-MM-dd
     */
    public static final String FORMAT_YYYYMMDD = "yyyy-MM-dd";

    /**
     * 从现在开始往前获取num天日期列表, 默认格式yyyy-MM-dd
     * @param num 获取日期列表天数
     * @return num天日期列表
     */
    public static List<String> getPeridDays(int num) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_YYYYMMDD);
        List<String> days = new ArrayList<>(num);
        // 获取当前时间
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -num + 1);
        while (num > 0) {
            days.add(sdf.format(cal.getTime()));
            // 日期数
            cal.add(Calendar.DAY_OF_YEAR, 1);
            num = num - 1;
        }
        return days;
    }
}
